<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');


Route::group(['middleware' => 'auth:api'], function() {
Route::get('/solicitudes/{numero_identidad}', 'SolicitudController@getDetail');
    Route::post('/solicitudes/confirmPayment', 'SolicitudController@paymentConfirm');
    Route::post('/documentos','DocumentosController@searchDocuments');
    Route::post('/solicitudes/nueva','SolicitudController@newSolicitud');
    Route::resource('universidades','UniversidadController',['only'=>['index']]);
    Route::post('/solicitudes/all', "SolicitudController@getSolicitudesOnAnyStatus");
    Route::get('/expediente/{expediente}','ExpedienteController@searchExpedData');
});

