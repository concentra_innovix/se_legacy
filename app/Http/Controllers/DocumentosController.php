<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use DB;

class DocumentosController extends Controller
{
    
      //This method search de documents asociated to  the  citizen and an university
    public function searchDocuments(Request $request)
    {
       
        
        $query = "select
		   
        doc.cddocument 'id',
        doc.nmtitle 'Nombre',
        doc.cdcategory 'categoria',
		Precio = (SELECT  top 1 tarifa  FROM dynMESCyTCL10 Tarifario 
        LEFT JOIN dynMESCyTCL14 TipoIes ON TipoIes.oid = Tarifario.OIDABCT2JDRNKYJ0JI 
        LEFT JOIN dynMESCyTCL03 Nac ON Nac.oid = Tarifario.OIDABCF20RG1ZBVLY7 
        WHERE TipoIes.TIPOIES = exp.TIPOIES and Nac.TIPONACIONALIDA = exp.tiponac and Tarifario.esempleado = 0),
        Vip = (SELECT  top 1 TARIFAVIP  FROM dynMESCyTCL10 Tarifario 
        LEFT JOIN dynMESCyTCL14 TipoIes ON TipoIes.oid = Tarifario.OIDABCT2JDRNKYJ0JI 
        LEFT JOIN dynMESCyTCL03 Nac ON Nac.oid = Tarifario.OIDABCF20RG1ZBVLY7 
        WHERE TipoIes.TIPOIES = exp.TIPOIES and Nac.TIPONACIONALIDA = exp.tiponac and Tarifario.esempleado = 0)  
        from
            DCDOCREVISION doc                                                                                                  
        join
            dccategory cat                                                                                                         
                on cat.cdcategory = doc.cdcategory                              
        join
            dynMESCyTHE05 exp                                                                  
                on exp.expednum = (
                    select distinct
                        docattrib.nmvalue              
                from
                    dcdocumentattrib docattrib              
                where
                    docattrib.cddocument = doc.cddocument                      
                    and docattrib.cdattribute = '11'    
                        
            )   
        left join dynmescytcl012 Nacionalidad on (exp.IDPAIS = Nacionalidad.OID)
        Left join DCDOCUMENTATTRIB docattr2 on docattr2.cddocument = doc.cddocument 
        where docattr2.cdattribute = '18' and docattr2.cdvalue = 4  
        and cat.CDCATEGORYOWNER = 35 
        and   exp.numeroidentidad = '".$request->numero_identidad."'   
        and exp.IESSIGLAS = '".$request->siglas_universidad."' 
        ";
       
        
        $documentos = DB::connection('sqlsrv')->select($query);
       
        if(count($documentos) == 0)
        {
            $result = ['Estado' => 'Error', 'Detalle' => 'No tiene documentos disponibles para legalizar de la universidad seleccionada.'];
            return $result;
           
        }
        {
            foreach($documentos as $documento)
            {
                    $documento->multiple = false;    
            }
            $request->vip = 0;
            if($request->vip == 1)
            {
                foreach($documentos as $documento)
                {
                    $documento->Precio = $documento->Vip;
                    unset($documento->Vip);
                }
            }
            else
            {
                foreach($documentos as $documento)
                {
                   unset($documento->Vip);
                }
            }
            return JsonResponse::create($documentos, 200, array('Content-Type'=>'application/json; charset=utf-8' ));
        }
       
    }

}
