<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UniversidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = "select  bidclasif 'siglas', ctituloclas 'nombre' from dynMescytCLgen where atipoclas = 'IES'";
        $universidades = DB::connection('sqlsrv')->select($query);
        return response()->json(['data' => $universidades,'code' => 200]);
    }

}
