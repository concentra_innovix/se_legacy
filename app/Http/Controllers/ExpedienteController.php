<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class ExpedienteController extends Controller
{
    public function searchExpedData(Request $request)
    {
        $query = "SELECT Expediente.OFICIONUMERO 'Mescytnumoficio', Expediente.IESNOMBRE 'MescytSiglasIES', Expediente.IESSIGLAS 'MescytSiglasIES',
                Expediente.CARRRERANOMBRE 'Mescytcarrera', CONCAT( Expediente.EXPED, ' ',Expediente.APELLIDO1, ' ',Expediente.APELIDO2) as Mescytnombsol,
                Expediente.NUMEROIDENTIDAD as Mescytidciuda, Expediente.EXPEDMATRICULA as Mescytmatricula, Expediente.EXPEDNUM as Mescytnumexp 
                FROM DYNMESCYTHE05 Expediente where Expediente.EXPEDNUM = '$request->expediente'";
        
        $expediente = DB::connection('sqlsrv')->select($query);

        if($expediente)
        {
            return response()->json(['success' => true, 'data' =>$expediente[0]],200);
        }
        return response()->json(['success' =>false, 'error' => 'expediente no encontrado'],404);
    }
}
