<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use DB;


class SolicitudController extends Controller
{
    //This method is used by Mesyct to obtain the Solicitud De Legalizacion´s Details
    public function getDetail($numero_identidad)
    {
        $numero_identidad = str_replace('-','',$numero_identidad);
        $query = "select Solicitud.NUMERODEIDENT 'NumeroIdentidad',Solicitud.SOLICITUDNUMERO 'IdSolicitud',Solicitud.estadosolicitud 'EstadoSolicitud', 
        Solicitud.MONEDAPG 'Moneda',Solicitud.EXPEDNUM 'IdExpediente',Solicitud.solicitnombre 'Nombres',Solicitud.solicapellido1 'Apellido1',Solicitud.solicapellido2 'Apellido2',
        CASE Solicitud.SERVICIOVIP 
            WHEN 1 
            THEN Solicitud.TARIFAVIP
            WHEN 0
            THEN Solicitud.TARIFADOC
            END as 'PrecioDocs',
         Solicitud.MONTOAPAGAR 'Monto', Solicitud.IESNOMBRE 'NombreIES', Solicitud.IESSIGLAS 'SiglasIES' from WFTASK WFT JOIN WFPROCESS WFP ON (WFP.CDPROCESS = WFT.IDPROCESS)
        JOIN DYNMESCyTHE03 Solicitud ON (Solicitud.SOLICITUDNUMERO = WFP.IDPROCESS)
        where nmrole = 'Cajero' and Solicitud.NUMERODEIDENT = '".$numero_identidad."' and Solicitud.estadosolicitud = 'Disponible para Pago'";
        
        //$query = "SELECT * FROM users WHERE 1";
        $cabecera = DB::connection('sqlsrv')->select($query);
        if(count($cabecera) == 0)
        {
            return Array('Estado'=>'Error','Detalle'=>'No se ha encontrado solicitudes Disponibles para Pago relacionadas a este ciudadano');
        }
       
        $query = "select DD.DOCUMENTO 'Documento', COUNT(DD.CATEGORIA) 'Cantidad' from dynMESCyTDTL01 DD where DD.SOLICITUDNUMERO = '".$cabecera[0]->IdSolicitud."'
        GROUP BY DD.CATEGORIA,DD.DOCUMENTO";
     
        $solicitudes = [];
       
      
 
        foreach($cabecera as $posicion=>$solicitud)
        {
            $query = "select DD.DOCUMENTO 'Documento', COUNT(DD.CATEGORIA) 'Cantidad' from dynMESCyTDTL01 DD where DD.SOLICITUDNUMERO = '".$solicitud->IdSolicitud."'
            GROUP BY DD.CATEGORIA,DD.DOCUMENTO";
            $detalle = DB::connection('sqlsrv')->select($query);
            $result = Array('ID'=>$solicitud->IdSolicitud,'DatosGenerales' => 
            Array(
                'NumeroIdentidad'=> $solicitud->NumeroIdentidad,
                'EstadoSolicitud'=> $solicitud->EstadoSolicitud,
                'Moneda'=> $solicitud->Moneda,
                'Nombres' => $solicitud->Nombres,
                'Apellidos' => $solicitud->Apellido1. ' '.$solicitud->Apellido2
                        ),
            'DetalleDocumentos' =>
                    Array(
                        'IdExpediente'  => $solicitud->IdExpediente,
                        'Documentos'    => $detalle,
                        'PrecioDocs'    => (int) $solicitud->PrecioDocs,
                        'Monto'         => (int) $solicitud->Monto,
                        'NombreIES'     =>  $solicitud->NombreIES,
                        'SiglasIES'     =>  $solicitud->SiglasIES
                                    
                    )
                    );
            array_push($solicitudes,$result);
        }
        return $solicitudes;
        
    }


    public function getSolicitudesOnAnyStatus(Request $request)
    {
        $query = "select Solicitud.NUMERODEIDENT 'NumeroIdentidad',Solicitud.SOLICITUDNUMERO 'IdSolicitud',Solicitud.estadosolicitud 'EstadoSolicitud', 
        Solicitud.MONEDAPG 'Moneda',Solicitud.EXPEDNUM 'IdExpediente',Solicitud.solicitnombre 'Nombres',Solicitud.solicapellido1 'Apellido1',Solicitud.solicapellido2 'Apellido2',
        CASE Solicitud.SERVICIOVIP 
            WHEN 1 
            THEN Solicitud.TARIFAVIP
            WHEN 0
            THEN Solicitud.TARIFADOC
            END as 'PrecioDocs',
         Solicitud.MONTOAPAGAR 'Monto', Solicitud.IESNOMBRE 'NombreIES', Solicitud.IESSIGLAS 'SiglasIES' from WFTASK WFT JOIN WFPROCESS WFP ON (WFP.CDPROCESS = WFT.IDPROCESS)
        JOIN DYNMESCyTHE03 Solicitud ON (Solicitud.SOLICITUDNUMERO = WFP.IDPROCESS)
        where nmrole = 'Cajero' and Solicitud.NUMERODEIDENT = '".$request->identification."'";
       
        $cabecera = DB::connection('sqlsrv')->select($query);
        if(count($cabecera) == 0)
        {
            return Array('Estado'=>'Error','Detalle'=>'No se ha encontrado solicitudes Disponibles para Pago relacionadas a este ciudadano');
        }
        $solicitudes = [];
       
      
 
        foreach($cabecera as $posicion=>$solicitud)
        {
            $query = "select DD.DOCUMENTO 'Documento', COUNT(DD.CATEGORIA) 'Cantidad' from dynMESCyTDTL01 DD where DD.SOLICITUDNUMERO = '".$solicitud->IdSolicitud."'
            GROUP BY DD.CATEGORIA,DD.DOCUMENTO";
            $detalle = DB::connection('sqlsrv')->select($query);
            
            $result = Array('ID'=>$solicitud->IdSolicitud,'DatosGenerales' => 
            Array(
                'NumeroIdentidad'=> $solicitud->NumeroIdentidad,
                'EstadoSolicitud'=> $solicitud->EstadoSolicitud,
                'Moneda'=> $solicitud->Moneda,
                'Nombres' => $solicitud->Nombres,
                'Apellidos' => $solicitud->Apellido1. ' '.$solicitud->Apellido2
               
                        ),
            'DetalleDocumentos' =>
                    Array(
                        'IdExpediente'  => $solicitud->IdExpediente,
                        'Documentos'    => $detalle,
                        'PrecioDocs'    => (int) $solicitud->PrecioDocs,
                        'Monto'         => (int) $solicitud->Monto,
                        'NombreIES'     =>  $solicitud->NombreIES,
                        'SiglasIES'     =>  $solicitud->SiglasIES
                                    
                    )
                    );
            array_push($solicitudes,$result);
        }
        return $solicitudes;
        
    }

    //This method confirm the payment of the documents to legalizate by te citizen
    public function paymentConfirm(Request $request)
    {
        $wf = new \nusoap_client(env('SE_WS_WF_WSDL'), 'wsdl');
        $wf->setCredentials(env('SE_USER'),env('SE_PASSWORD'));
        $parametros = array(
            'WorkflowID' =>  $request->NumeroSolicitud,
            'EntityID' => 'Mescythe03',
            'EntityAttributeList'=>[
                'EntityAttribute' => [
                    'EntityAttributeID' =>'c8numauthpago',
                    'EntityAttributeValue' => $request->NumeroAutorizacion,

                ],
                'EntityAttribute' => [
                    'EntityAttributeID' =>'estadosolicitud',
                    'EntityAttributeValue' => 'Pago Aplicado',
                    
                ],
                'EntityAttribute' => [
                    'EntityAttributeID' =>'tasa',
                    'EntityAttributeValue' => $request->tasa,
                    
                ]
            ]);
        $response = $wf->call('editEntityRecord',$parametros);
  
        if($response['Status'] == 'SUCCESS')
        {
            $executeResponse = $this->anvanzarSolicitud($request->NumeroSolicitud);
            $changeDocumentsState = $this->changeDocumentsState($request->NumeroSolicitud);

            $estado = Array('status'=>'success', 
                            'detail'=>utf8_decode($response['Detail']),
                            'solicitud'=>$request->NumeroSolicitud,
                            'executeActivityResponse' =>[
                                'status' =>strtolower($executeResponse['Status']),
                                'detail' => utf8_decode($executeResponse['Detail'])
                            ]);
        }
        else
        {
            $estado = Array('status'=>'failed', 
                            'detail'=>utf8_decode($response['Detail']),
                            'solicitud'=>$request->NumeroSolicitud);
        }
        return $estado;
    }

    //This method next the Solicitud to next step.
    public function anvanzarSolicitud($WorkflowID)
    {
        $wf = new \nusoap_client(env('SE_WS_WF_WSDL'), 'wsdl');
        $wf->setCredentials(env('SE_USER'),env('SE_PASSWORD'));
        $parameters = array(
            'WorkflowID' =>  $WorkflowID,
            'ActivityID' => 'MescytLDA1006',
            'ActionSequence'=>2);
        return  $wf->call('executeActivity',$parameters);

        
    }

    //Fill the solicitud´s minimal data required for the legalizacion.
    public function buildAttriuteList($exped_data)
    {
        $EntityAttributeList['EntityAttribute'] = array();
        foreach ($exped_data as $key => $arrayValue)
        {
            foreach ($arrayValue as $id => $value) 
            {
                $EntityAttribute = array('EntityAttributeID'=>$id,'EntityAttributeValue'=>$value);
                array_push($EntityAttributeList['EntityAttribute'],$EntityAttribute);
            }
        }
        $vipOrNot = array('EntityAttributeID'=>'serviciovip','EntityAttributeValue'=> 0);
        $solicitud_digital = array('EntityAttributeID'=>'expedientedigit','EntityAttributeValue'=> 1);
        $fecha = array('EntityAttributeID'=>'solicitudfecha','EntityAttributeValue'=> $date = date('Y-m-d'));
        $hora = array('EntityAttributeID'=>'solicitudhora','EntityAttributeValue'=> date("H:i"));
        array_push($EntityAttributeList['EntityAttribute'],$vipOrNot,$fecha,$hora);

        return $EntityAttributeList;
    }
    //Crea solicitud en SOFTEXPERT Y AGREGA LOS DOCUMENTOS
    public function newSolicitud(Request $request)
    {
       
        $documentos = $request->documents;
        if(count($documentos) == 0)
        {
            $data = array("Status" =>"FAILED",'Detail'=>'No se informaron documentos para esta solicitud, intente nuevamente.');
            return JsonResponse::create($data, 200, array('Content-Type'=>'application/json; charset=utf-8' ));
        }
            
        //return $request->documentos;
        $wf = new \nusoap_client(env('SE_WS_WF_WSDL'), 'wsdl');
        $wf->setCredentials(env('SE_USER'),env('SE_PASSWORD'));
        $wf->soap_defencoding = 'UTF-8';
        $wf->decode_utf8 = true;

        //1-Llena el formulario de legalizacion
        $expedient_data = $this->searchexpeddata($request->identification,$request->university);

        if($expedient_data == null)
        {
            $data = array("Status" =>"FAILED","Detail"=>'No se ha encontrado un expediente con los datos suministrados, revise los parámetros e intente nuevamente');
            return JsonResponse::create($data, 200, array('Content-Type'=>'application/json; charset=utf-8' ));
        }

        $attributeList = $this->buildAttriuteList($expedient_data);
        //2-Busca la informacion necesaria para insertar en el detalle de documento
        $entityList = [
            'Entity' =>[
                'EntityID' => 'Mescythe03',
                'EntityAttributeList' => $attributeList
            ]
        ];

        $parameters = ['ProcessID' => 'MescytLDA1', 'WorkflowTitle' => 'Solicitud de Legalizacion de Documentos', 'EntityList' =>$entityList];
        //dd($wf);
        $response = $wf->call('newWorkflowEditData', $parameters);
        //dd($wf->getError());
        $codigos = array();
        foreach($documentos as $documento)
        {
            array_push($codigos,$documento['id']);
        }

        $cddocs = implode(',',$codigos);

        //return $response;
        $docdetails = $this->searchdocsdetail($cddocs,$response['RecordID'],$documentos);
   
        if(count($docdetails) == 0)
        {
            $data = array("Status" =>"FAILED","Detail"=>'Los documentos suministrados no existen en Softexpert.');
            return JsonResponse::create($data, 200, array('Content-Type'=>'application/json; charset=utf-8' ));
        }
       
        $docs = json_decode(json_encode($docdetails), True);
      
        //Make the parameters for the newEntityChildRecord 
        $RecordList = $this->EntityChild($docs);
      
        //Parameters for calling the method
        $entityParam = array('WorkflowID' => $response['RecordID'],
            'MainEntityID' => 'MESCyTHE03',
            'ChildRelationshipID'=>'relsolicdetalle',
            'EntityRecordList' => $RecordList,
            );
        
      
        //Response
        $respuestaEntity = $wf ->call('newChildEntityRecordList',$entityParam);

        if($respuestaEntity['Status'] == "SUCCESS")
        {
            $data = array("Status" =>"SUCCESS",
            'RecordID' => $response['RecordID'],
            );
            return JsonResponse::create($data, 200, array('Content-Type'=>'application/json; charset=utf-8' ));
        }
        else
        {
            $data = array("Status" =>"FAILED");
            return JsonResponse::create($data, 200, array('Content-Type'=>'application/json; charset=utf-8' ));
        }
       
 
    }

    public function changeDocumentsState($workflowid)
    {

        $query = "SELECT documentsid FROM dynMescythe03 WHERE solicitudnumero ='".$workflowid."'";
        $documents = DB::connection('sqlsrv')->select($query);
        $documents = explode(',',$documents[0]->documentsid);

        $dc = new \nusoap_client(env('SE_WS_DC_WSDL'), 'wsdl');
        $dc->setCredentials(env('SE_USER'),env('SE_PASSWORD'));
        $dc->soap_defencoding = 'UTF-8';
        $dc->decode_utf8 = true;
        $responses = [];
        foreach ($documents as $document) {
            $attributes = [];
            $solicitud = [
                'iddocument' =>$document, 
                'idrevision' => '',
                'idattribute' => 'Mescytnumsoli',
                'vlattribute' => $workflowid  
            ];
            $estado = [
                'iddocument' =>$document, 
                'idrevision' => '',
                'idattribute' => 'Mescytestadodocum',
                'vlattribute' => 'Solicitado'  
            ];

            $response = $dc->call('setAttributeValue', $solicitud);
            array_push($responses, $response);
            $response = $dc->call('setAttributeValue', $estado);
            array_push($responses, $response);

        }

        return $responses;

    }

    //Build the struct for childs
    public function EntityChild($data)
    {
        $array['EntityRecord'] = [];
        foreach ($data as $key => $arrayValue)
        {
            $c = 0;
            foreach ($arrayValue as $id => $value) 
            {
                $EntityAttribute['EntityAttribute'][$c] = array('EntityAttributeID'=>$id, 'EntityAttributeValue' => $value);
                $c++;
            }
            $EntityAttributeList[$key]['EntityAttributeList'] =  $EntityAttribute;
        }
        $array['EntityRecord'] = $EntityAttributeList;
        return $array;
     }

    //Search expediente's data, passing an citizen_id  and university_acronym
    public function searchexpeddata($numero_identidad,$siglas_universidad)
    {
        $query = "SELECT TOP 1 Exp.apelido2 'solicapellido2', Exp.apellido1 'solicapellido1', Exp.exped 'solicitnombre', CONCAT(Exp.exped,' ',Exp.apellido1,' ',Exp.apelido2) as 
        'nombrecompleto', Exp.expedcelular 'solicitcelular', Exp.expedemail 'solicitemail', Exp.expedmatricula 'solicmatricula',Exp.expednum 'expednum', Exp.numeroidentidad 'numerodeident',
        Exp.oid 'oidexped', Exp.notasbody 'notasbody', 1 'expedientedigit', Exp.oficionumero 'oficionumero', Exp.lugardenaci 'lugardenaci', Exp.numerotitulo 'numerotitulo', Exp.fechatitulo 'fechatitulo',
        exp.TELEFONO 'solictelefono', Exp.estadocivil, Exp.paisnombre 'paisorigen', Exp.sexo, Exp.nofolio 'nofolio',Exp.nolibro 'nolibro',Exp.tipoies 'tipoies', Exp.carrreranombre 'carreranombe'
        FROM DYNMESCYTHE05  Exp 
        WHERE numeroidentidad = '".$numero_identidad."'  and Exp.IESSIGLAS = '".$siglas_universidad."'
        ORDER BY FECHAEXPEDIENTE DESC";    
        //dd($query);
        $data_expediente = DB::connection('sqlsrv')->select($query);
        if(count($data_expediente) == 0)
            return null;
        return $data_expediente;
    }



    //Search the minimal data requiered for the newChild relacionated to the solicitud
    public function searchdocsdetail($documentos,$idsolicitud,$dataDocuments)
    {  
        $query = "select
        doc.nmtitle 'documento',
		CAST(1 AS int) as cantidad,
		exp.IESSIGLAS 'siglasies',
		doc.CDCATEGORY 'categoria',
		exp.NUMEROIDENTIDAD 'cedula',
		doc.CDDOCUMENT 'codigodocu',
		Nacionalidad.NACIONALIDAD 'codigonacionali',
		'".$idsolicitud."' 'solicitudnumero',
		exp.TIPOIES,
		doc.DTINSERT 'fechadocumento',
		exp.expednum 'expednumdoc',
		doc.IDDOCUMENT 'iddocumento'
        from
            DCDOCREVISION doc 
                                                                                                            
        join
            dccategory cat                                                                                                         
                on cat.cdcategory = doc.cdcategory                              
        join
            dynMESCyTHE05 exp                                                                  
                on exp.expednum = (
                    select distinct
                        docattrib.nmvalue              
                from
                    dcdocumentattrib docattrib              
                where
                    docattrib.cddocument = doc.cddocument                      
                    and docattrib.cdattribute = '11'    
                        
            )   

        left join dynmescytcl012 Nacionalidad on (exp.IDPAIS = Nacionalidad.OID)


        where doc.CDDOCUMENT  IN (".$documentos.")";
        $detail = DB::connection('sqlsrv')->select($query);
        
        return $detail;
    }
}
